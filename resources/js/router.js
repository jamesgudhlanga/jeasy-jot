import Vue from 'vue';
import VueRouter from 'vue-router';

import ExampleComponent from './components/ExampleComponent';
import ContactsIndex from './views/ContactsIndex';
import ContactCreate from './views/ContactCreate';
import ContactShow from './views/ContactShow';
import ContactEdit from './views/ContactEdit';
import BirthdaysIndex from './views/BirthdaysIndex';

Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {path: '/', component: ExampleComponent},
        {path: '/contacts', component: ContactsIndex},
        {path: '/contacts/create', component: ContactCreate},
        {path: '/contacts/:id', component: ContactShow},
        {path: '/contacts/:id/edit', component: ContactEdit},
        {path: '/birthdays', component: BirthdaysIndex},
    ],
    mode: 'history'
});
