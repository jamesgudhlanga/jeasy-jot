<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Contact;
use App\User;
use Faker\Generator as Faker;

$factory->define(Contact::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName .' '. $faker->lastName,
        'email' => $faker->safeEmail,
        'birthday' => '02/12/1985',
        'company' => $faker->company,
        'user_id' => factory(User::class)
    ];
});
