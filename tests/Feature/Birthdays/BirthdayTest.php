<?php

namespace Tests\Feature\Birthdays;

use App\Contact;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BirthdayTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function contacts_with_birthdays_in_the_current_months_can_be_fetched()
    {
        $user = factory(User::class)->create();
        $thisMonthBirthday =  factory(Contact::class)->create([
           'user_id' => $user->id,
           'birthday' => now()->subYear()
        ]);
        $lastMonthBirthday =  factory(Contact::class)->create([
            'user_id' => $user->id,
            'birthday' => now()->subMonth()
        ]);

        $res = $this->get('/api/birthdays?api_token='.$user->api_token)
            ->assertJsonCount(1)
            ->assertJson([
                'data' => [
                    [
                        'data' =>[
                            'contact_id' => $thisMonthBirthday->id
                        ]
                    ]
                ]
            ]);
    }
}
