<?php

namespace Tests\Feature\Contacts;

use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;
use App\Contact;

class ContactsTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    /**
     * @test
     */
    public function an_unauthenticated_user_should_be_redirected_to_login()
    {
        $res = $this->post('/api/contacts',array_merge( $this->data(), ['api_token' => '']));
        $res->assertRedirect('/login');
        $this->assertCount(0, Contact::all());
    }

    /**
     * @test
     */
    public function a_list_of_contacts_can_be_fetched_for_the_authenticated_user()
    {
        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();

        $contact = factory(Contact::class)->create(['user_id' => $user->id]);
        $contact2 = factory(Contact::class)->create(['user_id' => $user2->id]);

        $res = $this->get('/api/contacts?api_token='.$user->api_token);
        $res->assertJsonCount(1)
            ->assertJson([
                'data' => [
                    [
                        'data' =>[
                            'contact_id' => $contact->id
                        ]
                    ]
                ]
            ]);
    }

    /**
     * @test
     */
    public function authenticated_user_can_add_a_contact()
    {
        $this->withoutExceptionHandling();

        $res = $this->post('/api/contacts', $this->data())
            ->assertStatus(Response::HTTP_CREATED);

        $contact = Contact::first();
        $this->assertEquals('James Gudhlanga', $contact->name);
        $this->assertEquals('jimmyneds@gmail.com', $contact->email);
        $this->assertEquals('02/12/1985', $contact->birthday->format('m/d/Y'));
        $this->assertEquals('Jeasy Solution', $contact->company);
        $res->assertJson([
            'data' => [
                'contact_id' => $contact->id
            ],
            'links' => [
                'self' => $contact->path()
            ]
        ]);
    }


    /**
     * @test
     */
    public function fields_are_required()
    {
        collect(['name', 'email', 'birthday', 'company'])
            ->each(function ($field) {
                $res = $this->post('/api/contacts', array_merge($this->data(), [$field => '']));
                $res->assertSessionHasErrors($field);
                $this->assertCount(0, Contact::all());
            });
    }

    /**
     * @test
     */
    public function email_must_be_valid()
    {
        $res = $this->post('/api/contacts', array_merge($this->data(), ['email' => 'not a valid email']));
        $res->assertSessionHasErrors('email');
        $this->assertCount(0, Contact::all());
    }

    /**
     * @test
     */
    public function birthdays_are_properly_stored()
    {
        $res = $this->post('/api/contacts', $this->data());
        $this->assertInstanceOf(Carbon::class, Contact::first()->birthday);
    }

    protected function data()
    {
        return [
            'name' => 'James Gudhlanga',
            'email' => 'jimmyneds@gmail.com',
            'birthday' => '02/12/1985',
            'company' => 'Jeasy Solution',
            'api_token' => $this->user->api_token
        ];
    }

    /**
     * @test
     */
    public function a_contact_ca_be_retrieved()
    {
        $contact = factory(Contact::class)->create(['user_id' => $this->user->id]);
        $res = $this->get('/api/contacts/'.$contact->id.'?api_token='.$this->user->api_token);

        $res->assertJson([
            'data' => [
                'contact_id' => $contact->id,
                'name' => $contact->name,
                'email' => $contact->email,
                'birthday' => $contact->birthday->format('m/d/Y'),
                'company' => $contact->company,
                'last_updated' => $contact->updated_at->diffForHumans()
            ]
        ]);
    }

    /**
     * @test
     */
    public function only_the_users_contacts_can_be_retrieved()
    {
        $contact = factory(Contact::class)->create(['user_id' => $this->user->id]);
        $user2 = factory(User::class)->create();
        $res = $this->get('/api/contacts/'.$contact->id.'?api_token='.$user2->api_token)
            ->assertStatus(403);
    }

    /**
     * @test
     */
    public function a_contact_ca_be_patched()
    {
        $contact = factory(Contact::class)->create(['user_id' => $this->user->id]);
        $res = $this->patch('/api/contacts/'.$contact->id, $this->data());
        $contact = $contact->fresh();

        $this->assertEquals('James Gudhlanga', $contact->name);
        $this->assertEquals('jimmyneds@gmail.com', $contact->email);
        $this->assertEquals('02/12/1985', $contact->birthday->format('m/d/Y'));
        $this->assertEquals('Jeasy Solution', $contact->company);

        $res->assertStatus(Response::HTTP_OK)
            ->assertJson([
            'data' => [
                'contact_id' => $contact->id
            ],
            'links' => [
                'self' => $contact->path()
            ]
        ]);
    }

    /**
     * @test
     */
    public function only_an_owner_can_patch_a_contact()
    {
        $contact = factory(Contact::class)->create();
        $user2 = factory(User::class)->create();
        $this->patch('/api/contacts/'.$contact->id, array_merge($this->data(), ['api_token' => $user2->api_token]))
            ->assertStatus(403);
    }

    /**
     * @test
     */
    public function a_contact_ca_be_deleted()
    {
        $contact = factory(Contact::class)->create(['user_id' => $this->user->id]);
        $this->delete('/api/contacts/'.$contact->id, ['api_token' => $this->user->api_token])
            ->assertStatus(Response::HTTP_NO_CONTENT);
        $this->assertCount(0, Contact::all());
    }

    /**
     * @test
     */
    public function only_the_owner_can_delete_a_contact()
    {
        $contact = factory(Contact::class)->create();
        $user2 = factory(User::class)->create();
        $this->delete('/api/contacts/'.$contact->id, ['api_token' => $user2->api_token])
            ->assertStatus(403);
    }
}
