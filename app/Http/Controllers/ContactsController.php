<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Resources\Contact as ContactResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class ContactsController
 * @package App\Http\Controllers
 */
class ContactsController extends Controller
{

    /**
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Contact::class);
        return ContactResource::collection(request()->user()->contacts);
    }

    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public  function store()
    {
        $this->authorize('create', Contact::class);
        $contact = request()->user()->contacts()->create($this->validateData());
        return (new ContactResource($contact))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @param Contact $contact
     * @return ContactResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Contact $contact)
    {
        $this->authorize('view', $contact);
        return new ContactResource($contact);
    }

    /**
     * @param Contact $contact
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Contact $contact)
    {
        $this->authorize('view', $contact);
        $contact->update($this->validateData());
        return (new ContactResource($contact))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * @return mixed
     */
    public function validateData()
    {
        return request()->validate([
            'name'=> 'required',
            'email' => 'required|email',
            'birthday' => 'required',
            'company' => 'required',
        ]);
    }

    /**
     * @param Contact $contact
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Contact $contact)
    {
        $this->authorize('view', $contact);
        $contact->delete();
        return response([], Response::HTTP_NO_CONTENT);
    }
}
